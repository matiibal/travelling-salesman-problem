import lombok.Data;
import lombok.val;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Data
public class NearestNeighborAlgorithm {
    private final List<Integer> road = new ArrayList<>();
    private Double cost = 0d;

    public void getInstance(List<Point> points) {
        var index = 0;
        val pointPresent = points.get(0);
        road.add(index);
        while (true) {
            val point = getPoint(points, index);
            points.remove(point);
            var minDistance = 0d;
            int i = 0;
            for (val pointI : points) {
                val distance = GeneratorPoint.calculateDistance(point, pointI);
                if (minDistance > distance || i == 0) {
                    minDistance = distance;
                    index = pointI.getId();
                }
                i++;
            }
            cost += minDistance;
            road.add(index);
            if (points.size() == 1) {
                val distance = GeneratorPoint.calculateDistance(points.get(0), pointPresent);
                cost += distance;
                road.add(pointPresent.getId());
                break;
            }
        }
    }

    private Point getPoint(List<Point> points, int index) {
        for (val p : points) {
            if (p.getId() == index) {
                return p;
            }
        }
       throw new IllegalArgumentException("ERROR");
    }

    void getPath() {
        val path = new StringJoiner("->");
        road.forEach(v -> path.add(String.valueOf(v)));
        System.out.println("\nScieżka algorytmu najbliższego sąsiada:\n" + path);
    }
}
