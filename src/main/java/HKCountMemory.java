import lombok.val;

public class HKCountMemory {
    public static void main(String[] args) {
        val generatorPoint = new GeneratorPoint();
        val points = generatorPoint.readPointsFromFile();
        generatorPoint.getListPointsMessage(points);
        val before = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        val dis = generatorPoint.getDistance(points);
        val heldKarpAlgorithm = new HeldKarpAlgorithm();
        System.out.println("Koszt drogi algorytmu Helda-Harpa: " + heldKarpAlgorithm.minCost(dis));
        val after = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        System.out.println("Zuzycie pamięci " + (after - before) + " B");
    }
}
