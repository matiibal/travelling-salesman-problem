import lombok.val;

import java.util.*;

public class HeldKarpAlgorithm {

    private static final double INFINITY = 100000000d;

    private static Set<Integer> createSet(int[] input, int pos) {
        if (pos == 0) {
            return new HashSet<>();
        }
        val set = new HashSet<Integer>();
        for (var i = 0; i < pos; i++) {
            set.add(input[i]);
        }
        return set;
    }

    public double minCost(double[][] distance) {

        //stores intermediate values in map
        val minCostDP = new HashMap<Index, Double>();
        val parent = new HashMap<Index, Integer>();

        val allSets = generateCombination(distance.length - 1);

        for (val set : allSets) {
            for (var currentVertex = 1; currentVertex < distance.length; currentVertex++) {
                if (set.contains(currentVertex)) {
                    continue;
                }
                Index index = Index.createIndex(currentVertex, set);
                var minCost = INFINITY;
                var minPrevVertex = 0;
                //to avoid ConcurrentModificationException copy set into another set while iterating
                val copySet = new HashSet<>(set);
                for (int prevVertex : set) {
                    double cost = distance[prevVertex][currentVertex] + getCost(copySet, prevVertex, minCostDP);
                    if (cost < minCost) {
                        minCost = cost;
                        minPrevVertex = prevVertex;
                    }
                }
                //this happens for empty subset
                if (set.size() == 0) {
                    minCost = distance[0][currentVertex];
                }
                minCostDP.put(index, minCost);
                parent.put(index, minPrevVertex);
            }
        }

        var set = new HashSet<Integer>();
        for (var i = 1; i < distance.length; i++) {
            set.add(i);
        }
        double min = Integer.MAX_VALUE;
        var prevVertex = -1;
        //to avoid ConcurrentModificationException copy set into another set while iterating
        val copySet = new HashSet<>(set);
        for (int k : set) {
            val cost = distance[k][0] + getCost(copySet, k, minCostDP);
            if (cost < min) {
                min = cost;
                prevVertex = k;
            }
        }

        parent.put(Index.createIndex(0, set), prevVertex);
        printTour(parent, distance.length);
        return min;
    }

    private void printTour(Map<Index, Integer> parent, int totalVertices) {
        val set = new HashSet<Integer>();
        for (int i = 0; i < totalVertices; i++) {
            set.add(i);
        }
        var start = Integer.valueOf(0);
        var stack = new LinkedList<Integer>();
        while (true) {
            stack.push(start);
            set.remove(start);
            start = parent.get(Index.createIndex(start, set));
            if (start == null) {
                break;
            }
        }
        val joiner = new StringJoiner("->");
        stack.forEach(v -> joiner.add(String.valueOf(v)));
        System.out.println("\nScieżka algorytmu Helda-Harpa:\n" + joiner);
    }

    private double getCost(Set<Integer> set, int prevVertex, Map<Index, Double> minCostDP) {
        set.remove(prevVertex);
        val index = Index.createIndex(prevVertex, set);
        val cost = minCostDP.get(index);
        set.add(prevVertex);
        return cost;
    }

    private List<Set<Integer>> generateCombination(int n) {
        var input = new int[n];
        for (int i = 0; i < input.length; i++) {
            input[i] = i + 1;
        }
        val allSets = new ArrayList<Set<Integer>>();
        var result = new int[input.length];
        generateCombination(input, 0, 0, allSets, result);
        allSets.sort(new SetSizeComparator());
        return allSets;
    }

    private void generateCombination(int[] input, int start, int pos, List<Set<Integer>> allSets, int[] result) {
        if (pos == input.length) {
            return;
        }
        val set = createSet(result, pos);
        allSets.add(set);
        for (int i = start; i < input.length; i++) {
            result[pos] = input[i];
            generateCombination(input, i + 1, pos + 1, allSets, result);
        }
    }

    private static class SetSizeComparator implements Comparator<Set<Integer>> {
        @Override
        public int compare(Set<Integer> o1, Set<Integer> o2) {
            return o1.size() - o2.size();
        }
    }
}
