import lombok.val;


public class Main {
    public static void main(String[] args) {
        val generatorPoint = new GeneratorPoint();
        val points = generatorPoint.generate(6);
        generatorPoint.getListPointsMessage();
        val start = System.currentTimeMillis();
        val threadHeldKarpAlgorithm = new Thread("HeldKarpAlgorithm") {
            public void run() {
                val dis = generatorPoint.getDistance(points);
                val heldKarpAlgorithm = new HeldKarpAlgorithm();
                System.out.println("Koszt drogi algorytmu Helda-Harpa: " + heldKarpAlgorithm.minCost(dis));
                System.out.println("Czas trwania algorytmu: " + (System.currentTimeMillis() - start) + " ms");
            }
        };
        val threadNearestNeighbor = new Thread("NearestNeighbor") {
            public void run() {
                val nearestNeighborAlgorithm = new NearestNeighborAlgorithm();
                nearestNeighborAlgorithm.getInstance(points);
                nearestNeighborAlgorithm.getPath();
                System.out.println("Koszt drogi algorytmu najbliższego sąsiada: " + nearestNeighborAlgorithm.getCost());
                System.out.println("Czas trwania algorytmu: " + (System.currentTimeMillis() - start) + " ms");
            }
        };
        threadHeldKarpAlgorithm.start();
        threadNearestNeighbor.start();
    }
}
