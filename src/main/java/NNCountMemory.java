import lombok.val;


public class NNCountMemory {
    public static void main(String[] args) {
        val generatorPoint = new GeneratorPoint();
        val points = generatorPoint.generate(16);
        generatorPoint.savePointToFile(points);
        generatorPoint.getListPointsMessage();
        val before_NN = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        val nearestNeighborAlgorithm = new NearestNeighborAlgorithm();
        nearestNeighborAlgorithm.getInstance(points);
        nearestNeighborAlgorithm.getPath();
        System.out.println("Koszt drogi algorytmu najbliższego sąsiada: " + nearestNeighborAlgorithm.getCost());
        val after_NN = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        System.out.println("Zuzycie pamięci " + (after_NN - before_NN) + " B");
    }

}
