import lombok.Data;
import lombok.val;

import java.util.Objects;
import java.util.Set;

@Data
public class Index {
    private int currentVertex;
    private Set<Integer> vertexSet;

    public static Index createIndex(int vertex, Set<Integer> vertexSet) {
        val i = new Index();
        i.currentVertex = vertex;
        i.vertexSet = vertexSet;
        return i;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        val index = (Index) o;
        if (currentVertex != index.currentVertex) return false;
        return Objects.equals(vertexSet, index.vertexSet);
    }

    @Override
    public int hashCode() {
        var result = currentVertex;
        result = 31 * result + (vertexSet != null ? vertexSet.hashCode() : 0);
        return result;
    }
}