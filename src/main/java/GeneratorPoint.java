import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.SneakyThrows;
import lombok.val;
import org.json.JSONArray;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GeneratorPoint {
    private final List<Point> listOfPoints = new ArrayList<>();

    static double calculateDistance(Point pointA, Point pointB) {
        return Math.abs(Math.sqrt((pointB.getX() - pointA.getX()) * (pointB.getX() - pointA.getX()) +
                (pointB.getY() - pointA.getY()) * (pointB.getY() - pointA.getY())));
    }

    public List<Point> generate(int n) {
        val random = new Random();
        for (int i = 0; i < n; i++) {
            listOfPoints.add(new Point(i, random.nextInt(200) + 1, random.nextInt(200) + 1));
        }
        return listOfPoints;
    }

    public List<Point> generateWithBreakDown(int n) {
        val random = new Random();
        for (int i = 0; i < n / 3; i++) {
            listOfPoints.add(new Point(i, random.nextInt(20) + 1, random.nextInt(20) + 1));
        }
        for (int i = n / 3; i < n; i++) {
            listOfPoints.add(new Point(i, random.nextInt(200) + 100, random.nextInt(200) + 300));
        }
        return listOfPoints;
    }

    double[][] getDistance(List<Point> listOfPoints) {
        val distances = new double[listOfPoints.size()][listOfPoints.size()];
        for (var i = 0; i < listOfPoints.size(); i++) {
            for (var j = 0; j < listOfPoints.size(); j++) {
                distances[i][j] = calculateDistance(listOfPoints.get(i), listOfPoints.get(j));
            }
        }
        return distances;
    }

    void getListPointsMessage() {
        System.out.println("Lista punktów:");
        listOfPoints.forEach(point ->
                System.out.println("Punkt " + point.getId() + " (" + point.getX() + "," + point.getY() + ")"));
    }

    void getListPointsMessage(List<Point> listOfPoints) {
        System.out.println("Lista punktów:");
        listOfPoints.forEach(point ->
                System.out.println("Punkt " + point.getId() + " (" + point.getX() + "," + point.getY() + ")"));
    }

    void savePointToFile(List<Point> listOfPoints) {
        val file = new File("points");
        try (PrintWriter printWriter = new PrintWriter(file)) {
            val jsonArray = new JSONArray(listOfPoints);
            printWriter.print(jsonArray);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows
    List<Point> readPointsFromFile() {
        val path = Paths.get("points");
        val read = Files.readAllLines(path).get(0);
        val points = new JSONArray(read);
        val gson = new Gson();
        val token = new TypeToken<List<Point>>() {};
        return gson.fromJson(String.valueOf(points), token.getType());
    }
}
