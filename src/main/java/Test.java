import lombok.val;

public class Test {
    public static void main(String[] args) {
        double avg = 0;
        int n = 400;
        for (int i = 0; i < n; i++) {
            val generatorPoint = new GeneratorPoint();

            val points = generatorPoint.generate(15);
            generatorPoint.getListPointsMessage();
            val dis = generatorPoint.getDistance(points);
            val heldKarpAlgorithm = new HeldKarpAlgorithm();
            System.out.println("Koszt drogi algorytmu Helda-Harpa: " + heldKarpAlgorithm.minCost(dis));
            val nearestNeighborAlgorithm = new NearestNeighborAlgorithm();
            nearestNeighborAlgorithm.getInstance(points);
            nearestNeighborAlgorithm.getPath();
            System.out.println("Koszt drogi algorytmu najbliższego sąsiada: " + nearestNeighborAlgorithm.getCost());

            double devation = (nearestNeighborAlgorithm.getCost()-heldKarpAlgorithm.minCost(dis))/heldKarpAlgorithm.minCost(dis);
            avg+=devation;
        }
        System.out.println("Błąd "+avg/n);
    }
}
